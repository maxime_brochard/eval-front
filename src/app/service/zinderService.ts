import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {People} from '../model/people';
import {MatchResult} from '../model/matchResult';
import {MatchCreator} from '../model/matchCreator';
import {MatchComponent} from '../match/match.component';
import {Match} from '../model/match';
import {Interest} from '../model/interest';

@Injectable()
export class ZinderService {
  constructor(private http: HttpClient) {
  }

  getPeoples(): Observable<People[]> {
    return this.http.get<People[]>('http://localhost:8088/zinder/profils');
  }

  addAMatch(id: string, matchToPost: MatchCreator){
    console.log(matchToPost);
    return this.http.post(`http://localhost:8088/zinder/profils/${id}/match`, matchToPost);
  }

  getMatchs(): Observable<Match[]> {
    return  this.http.get<Match[]>('http://localhost:8088/zinder/matchs');
  }

  getInterets() {
    return  this.http.get<Interest[]>('http://localhost:8088/zinder/interets');
  }
}
