import {Component, Input, OnInit} from '@angular/core';
import {ZinderService} from '../service/zinderService';
import {People} from '../model/people';
import {MatchCreator} from '../model/matchCreator';

@Component({
  selector: 'app-match-buttons',
  templateUrl: './match-buttons.component.html',
  styleUrls: ['./match-buttons.component.css']
})
export class MatchButtonsComponent implements OnInit {
  @Input() people: People;
  private isMatchSent: boolean;

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
  }

  sendMatch(b: boolean) {
    const toPost = new MatchCreator(b);
    this.zinderService.addAMatch(this.people.id, toPost).subscribe(
      () => this.isMatchSent = true,
      () => this.isMatchSent = false);
  }
}
