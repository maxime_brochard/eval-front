import {Component, Input, OnInit} from '@angular/core';
import {Match} from '../model/match';
import {ZinderService} from '../service/zinderService';
import {People} from '../model/people';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {
  @Input() match: Match;
  listOfPeoples: People[] = [];
  people: People;

  constructor(private zService: ZinderService) {
  }

  ngOnInit() {
    // récupérer la liste totale des personnes
    this.zService.getPeoples().subscribe(items => {
      for (const item of items['profils']) {
        this.listOfPeoples.push(item);
      }
    });
  }


}
