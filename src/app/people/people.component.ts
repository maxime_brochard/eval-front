import {Component, Input, OnInit} from '@angular/core';
import {Interest} from '../model/interest';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  @Input() people: PeopleComponent;
  interets: Interest[];
  id: string;
  nom: string;
  prenom: string;
  photoUrl: string;

  constructor() { }

  ngOnInit() {
    console.log(this.people.interets);

  }

}
