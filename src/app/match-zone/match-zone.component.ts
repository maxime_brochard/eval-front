import {Component, OnInit} from '@angular/core';
import {People} from '../model/people';
import {ZinderService} from '../service/zinderService';
import {Interest} from '../model/interest';


@Component({
  selector: 'app-match-zone',
  templateUrl: './match-zone.component.html',
  styleUrls: ['./match-zone.component.css']
})
export class MatchZoneComponent implements OnInit {
  listPeople: People[] = [];
  interets: Interest[] = [];

  constructor(private zService: ZinderService) {

  }

  ngOnInit() {
    this.zService.getPeoples().subscribe(items => {
      for (const item of items['profils']) {
        this.listPeople.push(item);
      }
    });

    this.zService.getInterets().subscribe(interets => {
    this.interets = interets;
    });
  }


}
