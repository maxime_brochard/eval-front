import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchZoneComponent } from './match-zone.component';

describe('MatchZoneComponent', () => {
  let component: MatchZoneComponent;
  let fixture: ComponentFixture<MatchZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
