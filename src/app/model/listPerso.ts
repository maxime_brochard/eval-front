import {People} from './people';

export class ListPerso {
  listPersoOfPeople: People[] = [];


  constructor(listPersoOfPeople: People[]) {
    this.listPersoOfPeople = listPersoOfPeople;
  }
}
