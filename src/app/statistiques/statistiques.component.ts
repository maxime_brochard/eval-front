import { Component, OnInit } from '@angular/core';
import {ZinderService} from '../service/zinderService';
import {MatchComponent} from '../match/match.component';
import {PeopleComponent} from '../people/people.component';
import {Match} from '../model/match';
import {People} from '../model/people';

@Component({
  selector: 'app-statistiques',
  templateUrl: './statistiques.component.html',
  styleUrls: ['./statistiques.component.css']
})
export class StatistiquesComponent implements OnInit {
matchs: Match[] = [];
peoples: People[] = [];


  constructor(private zService: ZinderService) { }

  ngOnInit() {
    this.zService.getMatchs().subscribe(listOfMatch => {
      for (const item of listOfMatch) {
        this.matchs.push(item);
      }
    });

  }
}
