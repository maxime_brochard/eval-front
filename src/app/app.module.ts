import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatchZoneComponent } from './match-zone/match-zone.component';
import { PeopleComponent } from './people/people.component';
import { ZinderService } from './service/zinderService';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { MatchButtonsComponent } from './match-buttons/match-buttons.component';
import {FormsModule} from '@angular/forms';
import { StatistiquesComponent } from './statistiques/statistiques.component';
import { MatchComponent } from './match/match.component';

const appRoutes: Routes = [
  {path: 'matchzone', component: MatchZoneComponent},
  {path: 'statistiques', component: StatistiquesComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    HeaderComponent,
    FooterComponent,
    MatchZoneComponent,
    PeopleComponent,
    MatchButtonsComponent,
    StatistiquesComponent,
    MatchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule
  ],
  providers: [ZinderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
